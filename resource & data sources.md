RESOURCE & DATA SOURCES
---

**Resource** permet de créer des ressources <br>

**data** permet d'interroger les ressources existantes sur AWS <br>

```tf
provider "aws" {
    region = "eu-west-1"
    access_key = "AKIA5JJ3NHA5O56RNWB5"
    secret_key = "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"
}

resource "aws_vpc" "dev-vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "dev-vpc-subnet-1" {
  vpc_id     = aws_vpc.dev-vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-west-1a"
}

data "aws_vpc" "default_vpc" {
    default = true
}

resource "aws_subnet" "dev-vpc-subnet-2" {
  vpc_id     = data.aws_vpc.default_vpc.id
  cidr_block = "172.31.1.0/24"
  availability_zone = "eu-west-1a"
}

output "dev-vpc-id" {
    value = aws_vpc.dev-vpc.id
}
```
