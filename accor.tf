terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.36.0"
    }
  }
}

provider "aws" {
    region = "eu-west-3"
}

variable public_key_ssh_location {}
variable private_key_ssh_location {}
variable my_ip {}
variable env_prefix {}

# -----------------------------------------------------
# Création de l'instance EC2 dans le VPC par défaut 
#1. Choisir une AMI
#2. Choisir le type d'instance
#3. Définir une clé SSH 

#4. Configurer les règles de sécurité I/O
#5. Associer une ip public à notre instance
#6. Déployer notre application
# Le User data vs Les Provisionners 

# Les modules 
# Quelques commandes utiles 
# -----------------------------------------------------


# Création de l'instance EC2

# Récupération dynamique de l'AMI
data "aws_ami" "latest_amz_linux_img" {
    most_recent      = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-kernel-*-x86_64-gp2"]
    }
}

# Création de la clé ssh
resource "aws_key_pair" "accor_ec2_key" {
    key_name   = "ec2-key"
    public_key = file(var.public_key_ssh_location)
} 


# Création des règles de sécurité
resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh_http"
  description = "Allow ssh/http inbound traffic"

  ingress {
    description      = "SSH from My IP"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.my_ip]
  }

  ingress {
    description      = "Http from Anywhere"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.env_prefix}-sg"
  }
}


resource "aws_instance" "accor_ec2" {

    #1. Choose AMI
    ami = data.aws_ami.latest_amz_linux_img.id

    #2. Choose instance type
    instance_type = "t2.micro"

    #3. Assign SSH key 
    key_name = aws_key_pair.accor_ec2_key.key_name

    #4. Assign SG
    vpc_security_group_ids = [aws_security_group.allow_ssh_http.id]

    #5. Associate public address
    associate_public_ip_address = true 


    # -----------------------------------------------------

    #6. Déployer notre application avec user_data
    # user_data = <<EOF
    #               #!/bin/bash
    #               sudo yum update -y
    #               sudo yum install docker -y
    #               sudo systemctl start docker
    #               sudo docker container run -d -p 8080:80 --name nginx nginx
    #             EOF

    #6. Déployer notre application avec user_data file
    #user_data = file("entrypoint.sh")


    # -----------------------------------------------------

    #7. Déployer notre application avec provisionner remote-exec

    # connection {
    #       type = "ssh"
    #       host = self.public_ip
    #       user = "ec2-user"
    #       private_key = file(var.private_key_ssh_location )
    # }

    # provisioner "remote-exec" {
    #   inline = [
    #       #!/bin/bash
    #       "sudo yum update -y",
    #       "sudo yum install docker -y",
    #       "sudo systemctl start docker",
    #       "sudo docker container run -d -p 8080:80 --name nginx nginx"
    #   ]
      
    # }


    # -----------------------------------------------------


    #8. Déployer notre application avec provisionner file
    connection {
          type = "ssh"
          host = self.public_ip
          user = "ec2-user"
          private_key = file(var.private_key_ssh_location )
    }

    provisioner "file" {
      source = "entrypoint.sh"
      destination = "/home/ec2-user/entrypoint.sh" 
    }

    provisioner "remote-exec" {
      inline = [
        "chmod +x /home/ec2-user/entrypoint.sh",
        "/home/ec2-user/entrypoint.sh",
      ]
    }

    tags = {
        Name = "${var.env_prefix}-ec2"
    }

}

output "instance_ip" {
    value = aws_instance.accor_ec2.public_ip
}

  

