VARIABLE 
---

```tf
provider "aws" {
    region = "eu-west-1"
}
# VARIABLE 
variable "vpc_subnet_cidr_1" {
    description = "subnet cidr"
    default = "xxx"
}

variable "dev_vpc_cidr" {
    description = "vpc cidr"
}

variable "vpc_subnet_cidr_2" {
    description = "vpc subnet 1"
}

# RESOURCE
resource "aws_vpc" "dev-vpc" {
  cidr_block = var.dev_vpc_cidr
}

resource "aws_subnet" "dev-vpc-subnet-1" {
  vpc_id     = aws_vpc.dev-vpc.id
  cidr_block = var.vpc_subnet_cidr_1
  availability_zone = "eu-west-1a"
}

data "aws_vpc" "default_vpc" {
    default = true
}

resource "aws_subnet" "dev-vpc-subnet-2" {
  vpc_id     = data.aws_vpc.default_vpc.id
  cidr_block = var.vpc_subnet_cidr_2
  availability_zone = "eu-west-1a"
}
```

**Première manière de déclarer une variable** <br>
`terraform apply` Afficher un prompt pour entrer la valeur de la variable _subnet_cidr_block_ <br>

**Deuxième manière de déclarer une variable** <br>
`terraform apply -var "subnet_cidr_block=172.31.48.0/20"` <br>

**Troisième manière de déclarer une variable** <br>
Créer le fichier **terraform.tfvars** qui sera automatiquement reconnu par terraform comme étant le fichier de variable  <br>

_terraform.tfvars_
```
vpc_subnet_cidr_1 = 172.31.48.0/20
dev_vpc_cidr = 10.0.0.0/16
vpc_subnet_cidr_2 = 10.0.1.0/24
```

`terraform apply -var-file terraform-dev.tfvars`

# Variable d'environnement
Les variables d'environnements peuvent servir à enregistrer les credentials à AWS (AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY) <br>

```sh
export AWS_ACCESS_KEY_ID=xxx
export AWS_SECRET_ACCESS_KEY=xxx
```

`export TF_VAR_avail_zone="eu-west-3b"`
```tf
provider "aws" {
    region = "eu-west-1"
}

# VARIABLE 
variable "avail_zone" {}

# RESOURCE
data "aws_vpc" "default_vpc" {
    default = true
}

resource "aws_subnet" "dev-vpc-subnet-2" {
  vpc_id     = data.aws_vpc.default_vpc.id
  cidr_block = var.vpc_subnet_cidr_2
  availability_zone = var.avail_zone
}
```



