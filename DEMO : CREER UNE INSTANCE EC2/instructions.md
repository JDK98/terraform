PROVISIONNER UNE INSTANCE EC2 AVEC TERRAFORM
---

✅ Créer un VPC <br>
✅ Créer un Sous réseau <br>
✅ Configurer Internet Gateway <br>
✅ Configurer les tables de routables <br>
✅ Configurer l'association de sous-reseau <br>
✅ Créer l'instance EC2 <br>
✅ Configurer les règles de sécurité <br>
✅ Ajouter une clé SSH <br>
✅ Démarrer un conteneur nginx <br>
