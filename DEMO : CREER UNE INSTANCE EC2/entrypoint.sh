#!/bin/bash
sudo apt update -y 
sudo apt install docker.io -y
sudo systemctl start docker
sudo usermod -aG docker ubuntu
docker container run -d -p 8080:80 --name nginx --restart always nginx
echo "<h1> Hello world from terraform user data </h1>" | sudo tee /home/ubuntu/index.html
docker cp /home/ubuntu/index.html nginx:/usr/share/nginx/html/
