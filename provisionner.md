PROVISIONNER
---

Les provisionner sont une manière d'exécuter des commandes sur le serveur provisionné

```tf
provider "aws" {
    region = "eu-west-1"
}

variable "subnet_cidr_block" {}
variable "vpc_cidr_block" {}
variable "avail_zone" {}
variable "env_prefix" {}
variable "my_ip" {}
variable "instance_type" {}
variable "public_ssh_location" {}
variable "private_key_location" {}

resource "aws_vpc" "dev-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
      Name: "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "dev-vpc-subnet-1" {
  vpc_id     = aws_vpc.dev-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
      Name: "${var.env_prefix}-subnet-1"
  }
}

resource "aws_route_table" "dev-vpc-route-table" {
  vpc_id = aws_vpc.dev-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dev-vpc-igw.id
  }
  tags = {
    Name = "${var.env_prefix}-rtb"
  }
}

resource "aws_internet_gateway" "dev-vpc-igw" {
  vpc_id = aws_vpc.dev-vpc.id

  tags = {
    Name = "${var.env_prefix}-igw"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.dev-vpc-subnet-1.id
  route_table_id = aws_route_table.dev-vpc-route-table.id
}

resource "aws_security_group" "ec2tf-sg" {
  name        = "ec2tf-sg"
  vpc_id      = aws_vpc.dev-vpc.id

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.my_ip]
  }

  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest-ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] 
}

resource "aws_key_pair" "ssh-key" {
  key_name   = "ec2tf-key"
  public_key = file(var.public_ssh_location)
}

resource "aws_instance" "ec2tf" {
  ami           = data.aws_ami.latest-ubuntu.id
  instance_type = var.instance_type
  subnet_id = aws_subnet.dev-vpc-subnet-1.id
  vpc_security_group_ids = [aws_security_group.ec2tf-sg.id]
  availability_zone = var.avail_zone
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

#   user_data = file("entrypoint.sh")

## PROVISIONNER ##
  connection {
      type = "ssh"
      host = self.public_ip
      user = "ubuntu"
      private_key = file(var.private_key_location)
  }

#   provisioner "file" {
#       source = "entrypoint.sh"
#       destination = "/home/ubuntu/entrypoint.sh"
#   }

  provisioner "remote-exec" {
    inline = [
        #!/bin/bash
        "sudo apt update -y", 
        "sudo apt install docker.io -y",
        "sudo systemctl start docker",
        "sudo usermod -aG docker ubuntu",
        "sudo docker container run -d -p 8080:80 --name nginx --restart always nginx"
    ]
# script = file("entrypoint.sh") 
  }

  tags = {
    Name = "${var.env_prefix}-ec2tf"
  }
}

output "instance_ip" {
    value = aws_instance.ec2tf.public_ip
}
```
