TERRAFORM COMMAND
---

`terraform plan` Fait un preview des actions sans executé <br>
`terraform apply` Execute les instructions <br>
`terraform apply -auto-approve` Execute sans prompt de confirmation <br>
`terraform destroy -target [resource-name]` Supprime la ressource spécifié <br>
`terraform destroy` Supprime toutes les ressources <br>
`terraform state` Permet d'obtenir des infossur nos ressources
