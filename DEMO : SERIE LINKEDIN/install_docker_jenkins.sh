#!/bin/bash
# Ce script sera éxécuté à la création du serveur
# Il installera dans un premier temps Docker puis il créera un conteneur Jenkins
# On fera un mapping de volume entre les sockets et binaires Docker de l'hôte ET des volumes à l'intérieur du conteneur 
# Ce mapping afin de rendre Docker CLI accessible à l'intérieur de notre conteneur 
# Cela nous sera particulièrement utile dans nos pipelines 

sudo apt update -y
sudo apt install docker.io -y
sudo docker container run -d -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):/usr/bin/docker --name jenkins --restart always jenkins/jenkins:lts
