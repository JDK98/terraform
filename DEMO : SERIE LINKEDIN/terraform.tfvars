# Votre adresse IP public
my_ip = "160.120.214.83/32"
# Le chemin absolu de votre clé SSH
ssh_pem_file_location = "/Users/JDK/.ssh/jenkinsInstanceKey.pem"
# Le type d'instance
instance_type = "t2.micro"
