provider "aws" {
    region = "eu-west-1"
}

variable my_ip {} 
variable ssh_pem_file_location {} 
variable instance_type {} 

###############################################################
########  CHECK DE LA DERNIERE VERSION AMI UBUNTU      ########

data "aws_ami" "latest_ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

###############################################################
########          REGLE DE SECURITE DE L'INSTANCE      ########

resource "aws_security_group" "jenkins_instance_sg" {
  name        = "jenkins_instance_sg"
  vpc_id      = "vpc-0692497bde46db756"

  # Autorise l'accès SSH à uniquement mon adresse IP
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.my_ip]
  }

  # Autorise l'accès via le port 8080 à tout le monde
  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  # Autorise l'accès via le port 50000 à tout le monde
  ingress {
    from_port        = 50000
    to_port          = 50000
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  # Autorise tout le trafic sortant
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "jenkins_instance_sg"
  }
}

###############################################################
########          CREATION DE LA CLE SSH               ########

resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "jenkins_instance_ssh_key" {
  # Crée la clé public sur AWS
  key_name   = "jenkinsInstanceKey"       
  public_key = tls_private_key.ssh_key.public_key_openssh

  # Crée la clé privé sur un fichier défini de notre machine hôte
  provisioner "local-exec" {    
    command = <<-EOT
      echo '${tls_private_key.ssh_key.private_key_pem}' > ${var.ssh_pem_file_location}
      chmod 400 ${var.ssh_pem_file_location}
    EOT
  }
}  

###############################################################
########            CREATION DE L'INSTANCE             ########

resource "aws_instance" "jenkins_instance" {
  ami           = data.aws_ami.latest_ubuntu.id
  instance_type = var.instance_type
  associate_public_ip_address = true
  key_name = aws_key_pair.jenkins_instance_ssh_key.key_name
  vpc_security_group_ids = [aws_security_group.jenkins_instance_sg.id]

  user_data = file("install_docker_jenkins.sh")

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = 15
    volume_type = "gp2"
    delete_on_termination = true
  }

  tags = {
    Name = "jenkins_instance"
  }
}

output "instance_ip" {
    value = aws_instance.jenkins_instance.public_ip
}





